FROM openjdk:8-jdk-alpine
ADD target/app.jar app.jar
ENTRYPOINT ["java","-Xmx64m","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]